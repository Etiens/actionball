﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Damage))]
[RequireComponent(typeof(Renderer))]
public class Orb : MonoBehaviour {

	public enum OrbState { Flying, ComingBack, Home }

	private Transform m_homeTransform;
	private Transform m_parent;
	private Vector3 m_velocity = Vector3.zero;
	private Rigidbody m_rigidBody;
    private float m_initialVelocity = 0;
    private GameObject m_owner = null;

    private float m_coolDownTimer = 0f;
    private bool m_damageOnReturn = false;

    private GlowColorChanger m_colorChanger;
    public GlowColorChanger ColorChanger { get { return m_colorChanger; } }
    
    private Damage m_damage;
    public Damage DamageComponent { get { return m_damage; } }

    private float m_baseReturnSpeed;

    private float m_returnSpeed;
    public float ReturnSpeed { get { return m_returnSpeed; } set { m_returnSpeed = value; } }

    private Color m_desiredColor;

    [SerializeField]
    private Collider m_physicsCollider = null;

    [SerializeField]
	private float m_brakeDuration = 1f;
	private float m_brakeTimer = 0f;



	#region properties
	private OrbState m_currentState = OrbState.Home;

	private Transform m_desiredPosition;
	public Transform DesiredPosition { get { return m_desiredPosition; } set { m_desiredPosition = value; } }

	private float m_comeBackTimer = 0f;
	public float ComeBackTimer { get { return m_comeBackTimer; } set { m_comeBackTimer = value; } }

	private bool m_inUse = false;
	public bool InUse{ get {return m_inUse || m_coolDownTimer > 0;} set {m_inUse = value;} }

    public bool CoolingDown {  get { return m_coolDownTimer > 0; } }

    #endregion // properties

    public void Initialize(Transform position, float speed, Transform parent, GameObject owner)
	{
		m_homeTransform = position;
		m_desiredPosition = m_homeTransform;
        m_baseReturnSpeed = speed;
        m_returnSpeed = speed;
		m_parent = parent;
        m_owner = owner;
		m_rigidBody = GetComponent<Rigidbody>();
        m_damage = GetComponent<Damage>();
        m_colorChanger = GetComponent<GlowColorChanger>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        UpdateStates();
        if (m_coolDownTimer > 0)
        {
            m_coolDownTimer -= Time.deltaTime;
            if (m_coolDownTimer < 0)
            {
                m_colorChanger.ChangeGlowPower(m_colorChanger.BaseGlow);
            }
        }
    }

    private void UpdateStates()
    {
        switch (m_currentState)
        {
            case OrbState.Home:
                transform.position = Vector3.SmoothDamp(transform.position, m_desiredPosition.position, ref m_velocity, Time.deltaTime, m_returnSpeed);
                break;

            case OrbState.Flying:
                m_comeBackTimer -= Time.deltaTime;
                if (ComeBackTimer < 0)
                {
                    m_brakeTimer = m_brakeDuration;
                    GoHome(m_damageOnReturn);
                }
                break;

            case OrbState.ComingBack:
                transform.position = Vector3.SmoothDamp(transform.position, m_desiredPosition.position, ref m_velocity, Time.deltaTime, m_returnSpeed);

                if (m_brakeTimer > 0)
                {
                    m_brakeTimer -= Time.deltaTime;
                    // TODO use squared magnitude
                    float speed = Mathf.Lerp(m_initialVelocity, 0, 1 - (m_brakeTimer / m_brakeDuration));
                    m_rigidBody.velocity = m_rigidBody.velocity.normalized * speed;
                }
                else
                {
                    m_rigidBody.velocity = Vector3.zero;
                }
                if (Vector3.SqrMagnitude(transform.position - m_desiredPosition.position) < 0.1)
                {
                    OnHome();
                }
                break;
        }
    }

    private void OnHome()
    {
        m_currentState = OrbState.Home;
        m_inUse = false;

        transform.parent = m_parent;

        m_rigidBody.velocity = Vector3.zero;
        m_rigidBody.constraints = RigidbodyConstraints.FreezeAll;
        transform.rotation = Quaternion.identity;

        m_physicsCollider.enabled = false;

        m_returnSpeed = m_baseReturnSpeed;

        m_damage.Deactivate();

        if (m_colorChanger.CurrentGlowColor != m_desiredColor 
            || m_colorChanger.CurrentGlowPower != 1f)
        {
            m_colorChanger.BaseColor = m_desiredColor;
            ResetColor();
        }
    }

	public void GoHome(bool canDamage = false)
	{
		m_desiredPosition = m_homeTransform;
		m_currentState = OrbState.ComingBack;
        m_initialVelocity = m_rigidBody.velocity.magnitude;
        m_physicsCollider.enabled = false;

        if (!canDamage)
        {
            m_damage.Deactivate();
        }
	}

	public void Shoot(Vector3 velocity, float timer, float damage, float damageTimer, bool canPierce = true, bool damageOnReturn = false){
        transform.parent = null;
		m_rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
		m_rigidBody.velocity = velocity;
		m_comeBackTimer = timer;
		m_currentState = OrbState.Flying;
        if (damageOnReturn)
        {
            m_damage.Activate(m_owner, damage);
        }
        else
        {
            if (!canPierce)
            {
                m_physicsCollider.enabled = true;
            }
            m_damage.Activate(m_owner, damage, damageTimer, canPierce);
        }
        m_damageOnReturn = damageOnReturn;
	}

    public void CoolDown(float duration)
    {
        m_colorChanger.ChangeGlowPower(0f, duration * 2, true);
        m_coolDownTimer = duration;
    }

    public void ChangeBaseColor(Color newColor)
    {
        m_desiredColor = newColor;
        if (m_currentState == OrbState.Home)
        {
            m_colorChanger.BaseColor = newColor;
            ResetColor();
        }
    }

    private void ResetColor()
    {
        if (!CoolingDown)
        {
            m_colorChanger.ChangeGlowColor(m_colorChanger.BaseColor, 0, false, true, true, 0.5f);
            m_colorChanger.ChangeGlowPower(m_colorChanger.BaseGlow, 0, false, true, true, 0.5f);
        }
    }

    public void ResetOrb()
    {
        GoHome(false);
        m_rigidBody.constraints = RigidbodyConstraints.FreezeAll;
    }

}
