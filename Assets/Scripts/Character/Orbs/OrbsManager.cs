﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class OrbsManager : MonoBehaviour {

    private NeonColorManager m_neonColorManager;

    [SerializeField]
    private int m_orbsCount = 5;

    [SerializeField]
    private Orb m_orbPrefab = null;

    [SerializeField]
    private float m_orbsRotationRadius = 1.5f;

    [SerializeField]
	private float m_orbsRotationSpeed = 50f;

    [SerializeField]
    private float m_orbsComebackSpeed = 10f;

    [SerializeField]
    private Transform m_orbsParentTransform = null;

    private List<Orb> m_orbs = new List<Orb>();
    private List<GameObject> m_orbPositions = new List<GameObject>();

    private float m_orbsRotationSpeedModifier = 1f;
    public float OrbsRotationSpeedModifier { get { return m_orbsRotationSpeedModifier; } set {m_orbsRotationSpeedModifier = value; } }

    private float m_orbsRotationSpeedModifierTimer = 0f;
	public float OrbsRotationModDuration { get { return m_orbsRotationSpeedModifierTimer; } set {m_orbsRotationSpeedModifierTimer = value; } }

	public int AvailableOrbs { get { return m_orbs.Count(x => !x.InUse); } }

    void Awake()
    {
        for (int i = 0; i < m_orbsCount; i++)
		{

			GameObject newOrbPosition = new GameObject();
			newOrbPosition.transform.position = transform.position + transform.forward * m_orbsRotationRadius;
			newOrbPosition.transform.RotateAround(transform.position, Vector3.up,	 (360 * i)/m_orbsCount);
			newOrbPosition.transform.parent = m_orbsParentTransform;
			m_orbPositions.Add(newOrbPosition);

			Orb newOrb = Instantiate(m_orbPrefab, transform.position, Quaternion.identity) as Orb;
			newOrb.Initialize(newOrbPosition.transform, m_orbsComebackSpeed, m_orbsParentTransform, gameObject);
			newOrb.transform.parent = m_orbsParentTransform;
			m_orbs.Add(newOrb);
        }
        m_neonColorManager = GetComponent<NeonColorManager>();
        m_neonColorManager.ColorChangedEvent.AddListener(OnColorChanged);
        
    }

	// Update is called once per frame
	void Update () {
		for (int i = 0; i < m_orbPositions.Count; i ++)
		{
			m_orbPositions[i].transform.RotateAround(transform.position, Vector3.up, Time.deltaTime * m_orbsRotationSpeed * m_orbsRotationSpeedModifier	);
		}
		if (m_orbsRotationSpeedModifierTimer < 0)
		{
			m_orbsRotationSpeedModifier = 1f;
		}
		else
		{
			m_orbsRotationSpeedModifierTimer -= Time.deltaTime;
		}

	}


	public bool GetOrbs(int amount, out List<Orb> orbs)
    {
        orbs = new List<Orb>();

        if (amount > AvailableOrbs) return false;

        orbs = m_orbs.Where(x => !x.InUse).OrderBy(x => Random.value).Take(amount).ToList();
        foreach(Orb orb in orbs) { orb.InUse = true; }

        return true;
	}

	public Orb GetOrb()
	{
		if(AvailableOrbs < 1) return null;
        
        Orb orb = m_orbs.Where(x => !x.InUse).OrderBy(n => Random.value).FirstOrDefault();
        orb.InUse = true;

        return orb;
	}

    public void OnColorChanged(NeonColor newColor)
    {
        foreach(Orb orb in m_orbs)
        {
            orb.ChangeBaseColor(GlobalSettings.Instance.NeonColorSettings.GetColor(newColor));
        }
    }

    public void RecallOrbs(float speed = 0f)
    {
        foreach(Orb orb in m_orbs)
        {
            if (speed > 0f)
            {
                orb.ReturnSpeed = speed;
            }
            orb.ResetOrb();
        }
    }

}
