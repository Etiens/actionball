﻿using UnityEngine;
using System.Collections;

public class YellowMovementState : CharacterState {

    #region Settings
    [SerializeField]
    float m_dashSpeed1 = 80f;
    [SerializeField]
    float m_dashSpeed2 = 50f;
    [SerializeField]
    float m_orbsSpeed1 = 4f;
    [SerializeField]
    float m_orbsSpeed2 = 2f;
    [SerializeField]
    ParticleSystem explosion = null;
    #endregion // Options

    OrbsManager m_orbManager;

    protected override void Awake()
    {
        base.Awake();
        m_orbManager = GetComponentInParent<OrbsManager>();
    }

    protected override bool CheckEnterCondition()
    {
        return base.CheckEnterCondition() 
            && m_controller.Inputs.Defensive.IsPressed 
            && m_controller.Inputs.GroundDirection != Vector3.zero;
    }

    public override bool CanEnterState
    {
        get
        {
            return base.CanEnterState && m_orbManager.AvailableOrbs > 0;
        }
    }

    public override void EnterState()
    {
        base.EnterState();
    }

    public override void ExitState()
    {
        base.ExitState();
    }

    protected override void Update()
    {
        base.Update();
        if (Enabled)
        {

        }
    }
}
