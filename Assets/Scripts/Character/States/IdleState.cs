﻿using UnityEngine;
using System.Collections;

public class IdleState : CharacterState {

	protected override void Awake ()
	{
		base.Awake ();
		m_hasDuration = false;
	}

	public override void EnterState ()
	{
		base.EnterState ();
		m_controller.CanTakeAction = true;
	}

    protected override bool CheckEnterCondition()
    {
        return false;
    }
}
