﻿using UnityEngine;
using System.Collections;

public class AIShootingState : CharacterState {
    #region Settings
    [SerializeField]
    private LaserProjectile m_projectilePrefab;

    [SerializeField]
    private float m_projSpeed;

    [SerializeField]
    private float m_projDamage;
    #endregion // Settings

    protected override void Awake()
    {
        base.Awake();
    }

    protected override bool CheckEnterCondition()
    {
        return base.CheckEnterCondition() && m_cooldownTimer <= 0;
    }

    public override void EnterState()
    {
        base.EnterState();
        m_controller.CanTakeAction = false;
        Shoot();
    }

    public override void ExitState()
    {
        base.ExitState();
        m_controller.CanTakeAction = true;
    }

    protected override void Update()
    {
        base.Update();
    }

    void Shoot()
    {
        Vector3 direction = LevelManager.Instance.Player.transform.position - transform.position;
        direction = direction.normalized * m_projSpeed;

        LaserProjectile proj = Instantiate(m_projectilePrefab, transform.position, Quaternion.identity) as LaserProjectile;
        proj.transform.forward = direction;
        proj.Shoot(gameObject, m_projDamage, 20f, direction);
    }
}
