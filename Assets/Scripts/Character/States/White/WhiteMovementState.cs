﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class WhiteMovementState : CharacterState {

	#region Settings
	[SerializeField]
	float m_dashSpeed = 50f;
    [SerializeField]
    float m_orbsSpeed1 = 4f;
    [SerializeField]
    float m_orbsSpeed2 = 2f;
	#endregion // Options

	Vector3 m_direction;
	Rigidbody m_rigidBody;
	OrbsManager m_orbsManager;

	protected override void Awake()
	{
		base.Awake();
		m_rigidBody = GetComponentInParent<Rigidbody>();
		m_orbsManager = GetComponentInParent<OrbsManager>();
	}

	public override void EnterState ()
	{
		base.EnterState();
		m_canExit = false;
        m_controller.CanTakeAction = false;
		m_direction = m_controller.Inputs.GroundDirection;

        if (m_orbsManager)
        {
            m_orbsManager.OrbsRotationSpeedModifier = m_orbsSpeed1;
            m_orbsManager.OrbsRotationModDuration = m_stateDuration;
        }

    }

	public override void ExitState ()
	{
		base.ExitState();

		if (m_orbsManager)
		{
			m_orbsManager.OrbsRotationSpeedModifier = m_orbsSpeed2;
			m_orbsManager.OrbsRotationModDuration = m_cooldown;
        }
        m_controller.CanTakeAction = true;
        m_rigidBody.velocity = Vector3.zero;
	}

	protected override void Update ()
	{
		base.Update();
		if (Enabled)
		{
			m_rigidBody.velocity = m_dashSpeed * m_direction;
		}
	}

    protected override bool CheckEnterCondition()
    {
        return base.CheckEnterCondition() && m_controller.Inputs.Defensive.IsPressed && m_controller.Inputs.GroundDirection != Vector3.zero;
    }



}
