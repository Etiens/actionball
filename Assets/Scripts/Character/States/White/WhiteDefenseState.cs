﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WhiteDefenseState : CharacterState {

	#region Settings
	[SerializeField]
	List<Transform> m_orbPositions = new List<Transform>();
    [SerializeField]
    float m_perfectBlockWindow = 0.1f;
    [SerializeField]
    int m_orbsDisabledOnBlock = 1;
    [SerializeField]
    int m_orbsDisabledOnPerfect = 0;
    [SerializeField]
    float m_orbsDisabledTime = 5f;
    [SerializeField]
    Collider m_collider = null;
    [SerializeField]
    GameObject m_shieldObject = null;
    #endregion // Options

    CameraController m_camera;
    OrbsManager m_orbsManager;
    List<Orb> m_orbs = new List<Orb>();


	protected override void Awake()
	{
		base.Awake();
		m_orbsManager = GetComponentInParent<OrbsManager>();
        m_camera = GetComponentInParent<CameraController>();
        m_collider.enabled = false;

    }

    public override bool CanEnterState
    {
        get
        {
            return base.CanEnterState && m_orbsManager.AvailableOrbs >= m_orbPositions.Count;
        }
    }

    public override void EnterState ()
	{
		base.EnterState();

        m_orbsManager.GetOrbs(m_orbPositions.Count, out m_orbs);

        for (int i = 0; i < m_orbs.Count; i++)
		{
			m_orbs[i].DesiredPosition = m_orbPositions[i];
        }

		m_canExit = false;
		m_controller.CanTakeAction = false;
		m_controller.CanHover = false;
        m_collider.enabled = true;
        m_shieldObject.SetActive(true);
        if (m_camera) { m_camera.CameraRotatesBody = false; }
        
	}

	public override void ExitState ()
	{
		base.ExitState();
		foreach(Orb orb in m_orbs)
		{
			orb.GoHome();
		}
		m_controller.CanTakeAction = true;
		m_controller.CanHover = true;
        m_collider.enabled = false;
        m_shieldObject.SetActive(false);
        if (m_camera) { m_camera.CameraRotatesBody = true; }
    }

	protected override void Update ()
	{
		base.Update();
		if (Enabled)
		{
			if (!m_controller.Inputs.Defensive.IsDown || m_orbs.Count(x => !x.CoolingDown) < 1)
			{
				m_canExit = true;
				m_controller.OnExitState();
			}
		}
	}

    protected override bool CheckEnterCondition()
    {
        return base.CheckEnterCondition() && m_controller.Inputs.Defensive.IsPressed && m_controller.Inputs.GroundDirection == Vector3.zero;
    }

    void OnTriggerEnter(Collider col)
    {
        Damage damage = col.GetComponent<Damage>();
        if (   damage 
            && damage.Active
            && (col.tag.Contains("Player") != tag.Contains("Player"))) //Verify that they are not in the same faction
        {
            damage.Deactivate();
            int orbsDisabled;
            if (m_stateDuration - StateTimer < m_perfectBlockWindow)
            {
                orbsDisabled = m_orbsDisabledOnPerfect;
            }
            else
            {
                orbsDisabled = m_orbsDisabledOnBlock;
            }

            var orbs = m_orbs.Where(x => !x.CoolingDown).OrderBy(n => Random.value).Take(orbsDisabled).ToList();
            foreach (Orb orb in orbs)
            {
                orb.CoolDown(m_orbsDisabledTime);
            }
        }
    }

}

