﻿using UnityEngine;
using System.Collections;

public class WhiteAttack1 : CharacterState {

    #region Settings

    [SerializeField]
    private float m_orbSpeed = 50f;

    [SerializeField]
    private float m_orbDuration = 0.5f;

    [SerializeField]
    private float m_orbReturnSpeed = 25f;

    [SerializeField]
    private float m_damage = 3f;

    #endregion //Settings
    
    private OrbsManager m_orbManager;
    private Rigidbody m_rigidBody;
    private Orb m_orb;

    protected override void Awake()
    {
        base.Awake();
        m_orbManager = GetComponentInParent<OrbsManager>();
        m_rigidBody = GetComponentInParent<Rigidbody>();

    }

    protected override bool CheckEnterCondition()
    {
        return base.CheckEnterCondition() && m_controller.Inputs.Attack1.IsPressed;
    }

    public override bool CanEnterState
    {
        get
        {
            return base.CanEnterState && m_orbManager.AvailableOrbs > 0;
        }
    }

    public override void EnterState()
    {
        base.EnterState();
        m_orb = m_orbManager.GetOrb();
        if (!m_orb)
        {
            m_controller.OnExitState();
            return;
        }
        m_canExit = false;
        m_orb.Shoot(m_controller.Inputs.AimingDirection * m_orbSpeed, m_orbDuration, m_damage, 0f);
        m_orb.ReturnSpeed = m_orbReturnSpeed;
        m_orb = null;


    }

    public override void ExitState()
    {
        base.ExitState();
        m_controller.CanTakeAction = true;
    }

    protected override void Update()
    {
        base.Update();
        if (Enabled)
        {
            if (m_controller.Inputs.Attack1.IsDown) m_nextState = this;
            else m_nextState = null;
        }

    }
}
