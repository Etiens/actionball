﻿using UnityEngine;
using System.Collections;

public class WhiteSpecialState : CharacterState
{
    #region Settings
    [SerializeField]
    private float m_comebackSpeed = 0f;
    #endregion // Settings

    OrbsManager m_orbManager;

    protected override void Awake()
    {
        base.Awake();
        m_orbManager = GetComponentInParent<OrbsManager>();
    }

    protected override bool CheckEnterCondition()
    {
        return base.CheckEnterCondition() && m_controller.Inputs.Special.IsPressed;
    }

    public override void EnterState()
    {
        base.EnterState();
        m_canExit = false;
        m_controller.CanTakeAction = false;
        m_orbManager.RecallOrbs(m_comebackSpeed);
    }

    public override void ExitState()
    {
        base.ExitState();
    }
}
