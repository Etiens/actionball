﻿using UnityEngine;
using System.Collections.Generic;


public class WhiteAttack2State : CharacterState {

    #region Settings
    [SerializeField]
	private float m_speedWhileShooting = 0.5f;

	[SerializeField]
	private Transform m_orbPosition = null;

	[SerializeField]
	private float m_minDamage = 1f;

	[SerializeField]
	private float m_maxDamage = 10f;

    private float m_orbDamage = 0f;

	[SerializeField]
	private float m_timeToChargeUp = 2f;

    [SerializeField]
	private float m_minProjectileSpeed = 50f;

	[SerializeField]
	private float m_maxProjectileSpeed = 100f;

	[SerializeField]
	private float m_flyingTime = 2f;
    #endregion //Settings

    private OrbsManager m_orbsManager;
	private CameraController m_camera;
	private Orb m_orb;
	private float m_chargeUpTimer = 0f;
    private float m_chargeUpBaseGlow = 0f;


	protected override void Awake ()
	{
		base.Awake ();
		m_orbsManager = GetComponentInParent<OrbsManager>();
		m_camera = GetComponentInParent<CameraController>();
	}

	public override bool CanEnterState {
		get {
			return base.CanEnterState && m_orbsManager.AvailableOrbs > 0;
		}
	}

	public override void EnterState ()
	{
		base.EnterState ();
		m_chargeUpTimer = 0f;

        m_orb = m_orbsManager.GetOrb();
		if (!m_orb)
		{
			m_controller.OnExitState();
			return;
		}
		m_orb.DesiredPosition = m_orbPosition;
		m_camera.ZoomIn();

		m_controller.CanTakeAction = true;
		m_controller.Navigation.SpeedMultiplier -= m_speedWhileShooting;

        m_orbDamage = m_minDamage;

        m_chargeUpBaseGlow = m_orb.ColorChanger.BaseGlow;

    }

	public override void ExitState ()
	{
		base.ExitState ();
        m_controller.Navigation.SpeedMultiplier += m_speedWhileShooting;
        m_camera.ZoomOut();
		if (m_orb)
		{
			m_orb.GoHome();
		}
	}

	protected override void Update ()
	{
		base.Update ();
		if (Enabled)
		{
			if(m_orb && !m_controller.Inputs.Attack2.IsDown)
			{
				float ratio = Mathf.Clamp(m_chargeUpTimer / m_timeToChargeUp, 0, 1);
				float speed = Mathf.Lerp(m_minProjectileSpeed, m_maxProjectileSpeed, ratio);
				m_orb.Shoot(m_controller.Inputs.AimingDirection * speed, m_flyingTime, m_orbDamage, 5f, false);
				m_orb = null;
				m_controller.OnExitState();
				return;
			}
			else
			{
				m_chargeUpTimer += Time.deltaTime;
				float ratio = Mathf.Clamp(m_chargeUpTimer / m_timeToChargeUp, 0, 1);
				m_orbDamage = Mathf.Lerp(m_minDamage, m_maxDamage, ratio);
                
                float newGlow = Mathf.Lerp(m_chargeUpBaseGlow, 5f, ratio);
                m_orb.ColorChanger.ChangeGlowPower(newGlow);
			}
		}
	}

    protected override bool CheckEnterCondition()
    {
        return base.CheckEnterCondition() && m_controller.Inputs.Attack2.IsPressed;
    }

}
