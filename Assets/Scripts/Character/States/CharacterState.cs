﻿using UnityEngine;

public class CharacterState : MonoBehaviour{

	[SerializeField]
	private string m_name = "";
	public string Name { get { return m_name; } }

	[SerializeField]
	protected float m_cooldown = 0f;
	protected float m_cooldownTimer = 0f;
    
    [SerializeField]
    protected NeonColor m_stateColor = NeonColor.None;

	private bool m_enabled;
	protected bool Enabled { get { return m_enabled; } }

	protected bool m_canEnter = true;
	public virtual bool CanEnterState {
        get
        {
            return m_canEnter 
                && m_cooldownTimer <= 0; 
        }
    }

	protected bool m_canExit = true;
	public virtual bool CanExitState { get { return m_canExit;} }

    protected GameCharacterController m_controller;
    protected NeonColorManager m_colorManager;

    [SerializeField]
	protected float m_stateDuration;
	private float m_stateTimer;
    protected float StateTimer { get { return m_stateTimer; } }
	protected bool m_hasDuration = true;

	protected CharacterState m_nextState = null;

	protected virtual void Awake()
	{
		m_controller = GetComponentInParent<GameCharacterController>();
        m_colorManager = GetComponentInParent<NeonColorManager>();
		m_hasDuration = m_stateDuration > Mathf.Epsilon;
	}


	public virtual void EnterState()
	{
		m_enabled = true;
		m_stateTimer = m_stateDuration;
	}

	public virtual void ExitState()
	{
		if (m_enabled)
		{	
			m_enabled = false;
			m_cooldownTimer = m_cooldown;
		}
	}

	protected virtual void Update()
	{
		if (m_enabled)
		{
			if (m_hasDuration)
			{
				m_stateTimer -= Time.deltaTime;
				if (m_stateTimer < 0)
				{
					m_canExit = true;
					m_controller.OnExitState(m_nextState);
					return;
				}
			}
		}
		else
		{
			m_cooldownTimer -= Time.deltaTime;
            if(CheckEnterCondition()) m_controller.ChangeState(this);
		}
	}

    protected virtual bool CheckEnterCondition()
    {
        return m_stateColor == NeonColor.None
                || (m_colorManager && m_colorManager.CurrentColor == m_stateColor);
    }


}
