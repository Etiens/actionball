﻿using UnityEngine;
using System.Collections;

public class AICharacterController : GameCharacterController {

    [SerializeField]
    private float m_maximumMovementSpeed = 25f;

    [SerializeField]
    private float m_movementSpeedDamping = 0.05f;

    [SerializeField]
    private float m_maximumRotationSpeed = 60f;

    private float m_speedModifier = 1f;

    public float CurrentMaxMovementSpeed { get { return m_maximumMovementSpeed * m_speedModifier; } }

    public float CurrentMaxRotationSpeed { get { return m_maximumRotationSpeed * m_speedModifier; } }

    private Vector3 m_desiredVelocity = Vector3.zero;
    public Vector3 DesiredVelocity { get { return m_desiredVelocity; } set { m_desiredVelocity = value; } }

    private Vector3 m_desiredOrientation = Vector3.zero;
    public Vector3 DesiredOrientation { get { return m_desiredOrientation; } set { m_desiredOrientation = value.normalized; } }

    private GameObject m_target = null;
    public GameObject Target { get { return m_target; } set { m_target = value; } }

    private Rigidbody m_rigidBody;

    protected override void Awake()
    {
        base.Awake();
        m_rigidBody = GetComponent<Rigidbody>();
    }

    protected override void Start()
    {
        base.Start();
        m_target = LevelManager.Instance.Player.gameObject;
    }

    private void LateUpdate()
    {
        if (m_life.IsDead)
        {
            Destroy(gameObject);
        }
        if (CanTakeAction)
        {
            if (m_desiredVelocity.sqrMagnitude > CurrentMaxMovementSpeed * CurrentMaxMovementSpeed)
            {
                m_desiredVelocity = m_desiredVelocity.normalized * CurrentMaxMovementSpeed;
            }

            m_rigidBody.velocity = Vector3.Slerp(m_rigidBody.velocity, m_desiredVelocity, m_movementSpeedDamping);

            Vector3 newForward = transform.forward;
            if (Vector3.Angle(m_desiredOrientation, transform.forward) > 0)
            {
                transform.forward = Vector3.RotateTowards(transform.forward, m_desiredOrientation, m_maximumRotationSpeed * Time.deltaTime * Mathf.Deg2Rad, 1f);
            }
        }
        m_desiredVelocity = Vector3.zero;
    }
}
