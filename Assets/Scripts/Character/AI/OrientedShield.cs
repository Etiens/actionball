﻿using UnityEngine;
using System.Collections;

public class OrientedShield : MonoBehaviour {

    [SerializeField]
    GameObject m_shield = null;

    [SerializeField]
    float m_rotationSpeed = 30f;
    Vector3 m_desiredDirection = Vector3.zero;

    [SerializeField]
    float m_regenerationSpeed = 5f;

    [SerializeField]
    float m_regenerationCooldown = 2f;
    float m_regenerationTimer;

    [SerializeField]
    bool m_canResurrect = true;

    [SerializeField]
    float m_resurrectionCooldown = 5f;
    float m_resurrectionTimer;

    private Life m_life;
    private AICharacterController m_controller;

    void Awake()
    {
        m_life = m_shield.GetComponent<Life>();
        m_controller = GetComponent<AICharacterController>();
        m_life.LifeChanged.AddListener(OnLifeChanged);
        m_regenerationTimer = m_regenerationCooldown;
    }

    void Update()
    {
        if (m_controller.CanTakeAction)
        {

            if (!m_life.IsDead)
            {
                m_shield.SetActive(true);
                m_regenerationTimer -= Time.deltaTime;
                if (m_regenerationTimer < 0f)
                {
                    m_life.ModifyLife(m_regenerationSpeed * Time.deltaTime, gameObject, false);
                }
                if (m_controller.Target)
                {
                    m_desiredDirection = m_controller.Target.transform.position - transform.position;
                    m_shield.transform.forward = Vector3.RotateTowards(m_shield.transform.forward, m_desiredDirection, m_rotationSpeed * Mathf.Deg2Rad * Time.deltaTime, 1f);
                }
            }
            else
            {
                if (m_canResurrect)
                {
                    m_resurrectionTimer -= Time.deltaTime;
                    if (m_resurrectionTimer < 0f)
                    {
                        m_shield.SetActive(true);
                        m_life.ModifyLife(1f, gameObject, true);
                        m_regenerationTimer = 0f;
                    }
                }
            }
        }
        else
        {
            m_shield.SetActive(false);
        }
    }

    void OnLifeChanged(LifeChangedEventArgs args)
    {
        if (args.amountChanged < 0f)
        {
            m_regenerationTimer = m_regenerationCooldown;
        }
        if (m_life.IsDead)
        {
            m_resurrectionTimer = m_resurrectionCooldown;
            m_shield.SetActive(false);
        }
    }
}
