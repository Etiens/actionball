﻿using UnityEngine;
using System.Collections;

public class CircleAroundTarget : MonoBehaviour {

    [SerializeField]
    private float m_circlingSpeed = 10f;

    [SerializeField]
    private float m_minCirclingDistance = 10f;

    [SerializeField]
    private float m_maxCirclingDistance = 15f;

    [SerializeField]
    private float m_timeBeforeDirectionChange = 0f;
    private float m_directionChangeTimer;
    private bool m_directionChanged = false;

    private AICharacterController m_controller;

    private void Awake()
    {
        m_controller = GetComponent<AICharacterController>();
        m_directionChangeTimer = m_timeBeforeDirectionChange;
    }

    private void Update()
    {
        if (m_controller.CanTakeAction)
        {
            if (m_controller.Target)
            {
                if (Vector3.SqrMagnitude(transform.position - m_controller.Target.transform.position) > m_maxCirclingDistance * m_maxCirclingDistance)
                {
                    Vector3 direction = m_controller.Target.transform.position - transform.position;
                    direction.y = 0;
                    m_controller.DesiredVelocity += direction.normalized * m_circlingSpeed;
                }
                else if (Vector3.SqrMagnitude(transform.position - m_controller.Target.transform.position) < m_minCirclingDistance * m_minCirclingDistance)
                {
                    Vector3 direction = transform.position - m_controller.Target.transform.position;
                    direction.y = 0;
                    m_controller.DesiredVelocity += direction.normalized * m_circlingSpeed;
                }
                else
                {
                    Vector3 direction = Vector3.Cross(m_controller.Target.transform.position - transform.position, Vector3.up).normalized;
                    m_controller.DesiredVelocity += direction * m_circlingSpeed * (m_directionChanged ? -1f : 1f);
                }
            }

            if (m_directionChangeTimer > 0)
            {
                m_directionChangeTimer -= Time.deltaTime;
                if (m_directionChangeTimer <= 0)
                {
                    m_directionChangeTimer = m_timeBeforeDirectionChange;
                    m_directionChanged = !m_directionChanged;
                }
            }
        }
    }
}
