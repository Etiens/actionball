﻿using UnityEngine;
using System.Collections;

public class Boss1Panel : MonoBehaviour {

    Life m_life;
    Boss1Controller m_boss;

    // Hover variables
    [SerializeField]
    float m_hoverSpeed = 1f;

    [SerializeField]
    float m_hoverAmplitude = 0.25f;

    bool m_overrideHover = false;
    Vector3 m_initialDirection;
    float m_currentOffset;
    float m_time = 0f;


    void Awake()
    {
        m_life = GetComponent<Life>();
        m_boss = GetComponentInParent<Boss1Controller>();
        m_life.LifeChanged.AddListener(OnLifeChanged);
        m_initialDirection = transform.localPosition;
    }

    void OnLifeChanged(LifeChangedEventArgs args)
    {
        if (m_life.IsDead)
        {
            m_boss.OnPanelDestroyed(this);
            gameObject.SetActive(false);
        }
    }

    void Update()
    {
        if (!m_overrideHover)
        {
            m_time += Time.deltaTime;
            // Add one to prevent negative values
            m_currentOffset = (Mathf.Cos(m_time * m_hoverSpeed) + 1f) * m_hoverAmplitude;
            transform.localPosition = m_initialDirection + m_initialDirection.normalized * m_currentOffset;
        }
    }

    public void Open()
    {

    }

    public void Close()
    {

    }
}
