﻿using UnityEngine;

public class Boss1Controller : AICharacterController
{
    private Boss1Panel [] m_panels;
    private Boss1Panel m_facingPanel = null;
    private int m_panelCount;
    private Vector3 m_deadPanelOrientation = Vector3.zero;

    protected override void Awake()
    {
        base.Awake();
        m_panels = GetComponentsInChildren<Boss1Panel>();
        m_panelCount = m_panels.Length;
    }

    public void OnPanelDestroyed(Boss1Panel panel)
    {
        m_panelCount--;
        m_facingPanel = null;
        m_deadPanelOrientation += transform.InverseTransformDirection(panel.transform.position - transform.position).normalized;
    }

    protected override void Update()
    {
        base.Update();
        if (m_deadPanelOrientation == Vector3.zero)
        {
            if(m_facingPanel == null)
            {
                for (int i = 0; i < m_panels.Length; i++)
                {
                    if (m_panels[i].gameObject.activeSelf)
                    {
                        m_facingPanel = m_panels[i];
                        break;
                    }
                }
            }
            if (m_facingPanel)
            {
                Vector3 lastDir = transform.forward;
                transform.forward = Target.transform.position - transform.position;
                Vector3 direction = m_facingPanel.transform.position - transform.position;
                DesiredOrientation = direction;
                transform.forward = lastDir;                
            }
        }
        else
        {
            // Store current direction
            Vector3 lastDir = transform.forward;
            // Temporarily orient the boss towards the player.
            transform.forward = Target.transform.position - transform.position;
            Vector3 direction;
            // Reflect the direction of the dead panels so we know where we want the cube to rotate to in order for
            // the vulnerable spots to be hidden.
            if (m_deadPanelOrientation == Vector3.up)
            {
                direction = Vector3.Reflect(transform.TransformDirection(m_deadPanelOrientation), transform.TransformDirection(Vector3.up));
            }
            else if (m_deadPanelOrientation == Vector3.down)
            {
                // no idea what's going on here, but this works well enough for now.
                direction = Vector3.Reflect(transform.TransformDirection(m_deadPanelOrientation), Vector3.up);
            }
            else
            {
                direction = Vector3.Reflect(transform.TransformDirection(m_deadPanelOrientation), transform.TransformDirection(Vector3.right));
            }
            // Inverse the direction so that the weak spots are away from the player and not facing him.
            DesiredOrientation = -direction;
            // Set the direction back to what it was.
            transform.forward = lastDir;
        }
    }
}
