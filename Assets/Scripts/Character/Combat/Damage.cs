﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class Damage : MonoBehaviour
{
    private bool m_isActive = false;
    public bool Active { get { return m_isActive; } }

    private List<Life> m_targetsHit = new List<Life>();
    
    private float m_activationTimer = 0f;
    
    private float m_damage = 0f;

    private bool m_goesThrough = true;

    [SerializeField]
    private GameObject m_owner = null;
    public GameObject Owner { get { return m_owner; } }
    
    void OnTriggerEnter(Collider col)
    {
        Life life = col.gameObject.GetComponentInParent<Life>();
        if (m_isActive
            && col.tag.Contains("Hurtbox")
            && (col.tag.Contains("Player") != tag.Contains("Player")) //Verify that they are not in the same faction
            && life 
            && !m_targetsHit.Contains(life))
        {
            m_targetsHit.Add(life);
            life.ModifyLife(-m_damage, m_owner);
            if (!m_goesThrough)
            {
                Deactivate();
            }
        }
        else if(m_isActive && !m_goesThrough && col.tag.Contains("Terrain") )
        {
            Deactivate();
        }
    }

    public void Activate(GameObject owner, float damage, float duration = 0, bool canPierce = true)
    {
        m_isActive = true;
        m_activationTimer = duration;
        m_damage = damage;
        m_owner = owner;
        m_goesThrough = canPierce;

    }

    public void Deactivate()
    {
        m_targetsHit.Clear();
        m_isActive = false;
        m_activationTimer = 0;
    }

    private void Update()
    {
        if (m_activationTimer > 0)
        {
            m_activationTimer -= Time.deltaTime;
            if (m_activationTimer <= 0)
            {
                Deactivate();
            }
        }
    }
}

