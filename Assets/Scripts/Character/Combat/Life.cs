﻿using UnityEngine;
using System.Collections.Generic;

public class LifeChangedEventArgs
{
    public float amountChanged;
    public GameObject source;
    public GameObject master;
}

public class Life : MonoBehaviour {

    [SerializeField]
    private float m_currentLife = 100;
    public float CurrentLife { get { return m_currentLife; } }

    [SerializeField]
    private float m_maxLife = 100;
    public float MaxLife { get { return m_maxLife; } }
    
    public bool IsDead { get { return m_currentLife <= Mathf.Epsilon; } }

    [SerializeField]
    private bool m_isInvincible = false;
    public bool Invincible { get { return m_isInvincible; } set { m_isInvincible = value; } }

    private EventWithArg<LifeChangedEventArgs> m_lifeChangedEvent = new EventWithArg<LifeChangedEventArgs>();
    public EventWithArg<LifeChangedEventArgs> LifeChanged { get { return m_lifeChangedEvent; } }
    
    private void Awake()
    {
        m_currentLife = MaxLife;
    }

    public void ModifyLife(float amount, GameObject source, bool canResurrect = false)
    {
        if (amount == 0)
            return;

        LifeChangedEventArgs args = new LifeChangedEventArgs();
        float previousLife = m_currentLife;
        args.master = gameObject;
        args.source = source;
        
        if ((amount > Mathf.Epsilon && IsDead && !canResurrect) || ((amount < -Mathf.Epsilon && (IsDead || m_isInvincible))))
        {
            return;
        }
        else
        {
            m_currentLife += amount;
            m_currentLife = Mathf.Clamp(m_currentLife, 0, MaxLife);
            args.amountChanged = m_currentLife - previousLife;
            m_lifeChangedEvent.Invoke(args);
        }
    }

}
