﻿using UnityEngine;
using System.Collections;

public class LaserProjectile : MonoBehaviour {

    private Damage m_damage;
    private Rigidbody m_rigidBody;

    void Awake()
    {
        m_damage = GetComponentInChildren<Damage>();
        m_rigidBody = GetComponentInChildren<Rigidbody>();
    }

    void Start()
    {
    }
	// Update is called once per frame
	void Update ()
    {
        if (!m_damage.Active)
        {
            Destroy(gameObject);
        }
	}

    public void Shoot(GameObject owner, float damage, float duration, Vector3 velocity)
    {
        m_damage.Activate(owner, damage, duration, false);
        m_rigidBody.velocity = velocity;
    }
}
