﻿using UnityEngine;
using System.Collections;

public class Hover : MonoBehaviour {

	[SerializeField]
	private float m_amplitude = 0.5f;

	[SerializeField]
	private float m_speed = 1f;

	private float m_heightMod;
	private float m_time = 0f;

    private GameCharacterController m_controller;

    void Awake()
    {
        m_controller = GetComponentInParent<GameCharacterController>();
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (m_controller.CanHover)
		{
			m_time += Time.deltaTime;
			m_heightMod = Mathf.Cos(m_time * m_speed) * m_amplitude;
			transform.localPosition = new Vector3(0, m_heightMod, 0);
		}
	}
}
