﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(GameCharacterController))]
public class Navigation : MonoBehaviour {

	private Rigidbody m_rigidBody;
	private GameCharacterController m_controller;

	[SerializeField]
	private float m_walkingSpeed = 10f;

	private float m_currentSpeedMultiplier = 1f;
	public float SpeedMultiplier { get { return m_currentSpeedMultiplier; } set { m_currentSpeedMultiplier = value; } }


	void Awake()
	{
		m_rigidBody = GetComponent<Rigidbody>();	
		m_controller = GetComponent<GameCharacterController>();	
	}
	
	// Update is called once per frame
	void  Update () {
		if (m_controller.CanTakeAction)
		{
			m_rigidBody.velocity = m_walkingSpeed * m_currentSpeedMultiplier * m_controller.Inputs.GroundDirection;
		}
	}
}
