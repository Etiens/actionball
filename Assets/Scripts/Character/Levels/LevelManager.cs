﻿using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    [SerializeField]
    PlayerCharacterController m_mainCharacterPrefab = null;
    PlayerCharacterController m_mainCharacterRef = null;
    public PlayerCharacterController Player { get { return m_mainCharacterRef; } }

    [SerializeField]
    Transform m_spawnPoint = null;


    void Awake()
    {
        m_mainCharacterRef = Instantiate(m_mainCharacterPrefab, m_spawnPoint.position, m_spawnPoint.rotation) as PlayerCharacterController;
        Cursor.visible = false;
    }
}
