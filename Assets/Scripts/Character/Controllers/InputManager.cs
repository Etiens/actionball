﻿using UnityEngine;


public class ButtonInput
{
    protected float m_previousValue;

    public float Value { get; set; }
    public bool IsDown { get; protected set; }
    public bool IsPressed { get; protected set; }
    public bool IsReleased { get; protected set; }

    public virtual void Update()
    {
        IsDown = Value > 0;
        IsPressed = Value > 0 && m_previousValue <= 0;
        IsReleased = Value <= 0 && m_previousValue > 0;

        m_previousValue = Value;
    }

    public virtual void Reset()
    {
        Value = 0;
    }
}

public class InputManager
{
    public ButtonInput Defensive = new ButtonInput();
    public ButtonInput Attack1 = new ButtonInput();
    public ButtonInput Attack2 = new ButtonInput();
    public ButtonInput Special = new ButtonInput();
    public ButtonInput NeonColorNext = new ButtonInput();
    public ButtonInput NeonColorPrevious = new ButtonInput();
    public Vector3 GroundDirection;
    public Vector3 Forward;
    public Vector3 AimingDirection;

    public void Update()
    {

        Defensive.Update();
        Attack1.Update();
        Attack2.Update();
        Special.Update();
        NeonColorNext.Update();
        NeonColorPrevious.Update();
    }

    public void Reset()
    {
        Defensive.Reset();
        Attack1.Reset();
        Attack2.Reset();
        Special.Reset();
        NeonColorNext.Reset();
        NeonColorPrevious.Reset();
        GroundDirection = Vector3.zero;
        Forward = Vector3.zero;
        AimingDirection = Vector3.zero;
    }
}
