﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CameraController))]
public class PlayerCharacterController : GameCharacterController {

	private CameraController m_camera;
    public CameraController Camera { get { return m_camera; } }

    protected override void Awake()
    {
    	base.Awake();
        m_camera = GetComponent<CameraController>();
    }

	
	// Update is called once per frame
	protected override void Update () {
        base.Update();
		Inputs.AimingDirection = Camera.CameraTransform.forward.normalized;
        Inputs.Forward = new Vector3(Camera.CameraArm.transform.forward.x, 0, Camera.CameraArm.transform.forward.z).normalized;

        Inputs.GroundDirection = Camera.CameraArm.transform.TransformDirection(new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")));
        Inputs.GroundDirection = new Vector3(Inputs.GroundDirection.x, 0, Inputs.GroundDirection.z).normalized;

        Inputs.Defensive.Value = Input.GetAxis("Defensive");
        Inputs.Attack1.Value = Input.GetAxis("Attack1");
        Inputs.Attack2.Value = Input.GetAxis("Attack2");
        Inputs.Special.Value = Input.GetAxis("Special");
        Inputs.NeonColorNext.Value = Input.GetAxis("NextColor");
        Inputs.NeonColorPrevious.Value = Input.GetAxis("PreviousColor");
    }

	void OnGUI()
	{
		string orbs = "Available Orbs: " + GetComponent<OrbsManager>().AvailableOrbs;
        string life = "Current Life: " + GetComponent<Life>().CurrentLife;
		GUI.Label(new Rect(10, 10, 120, 20), m_currentState.Name);
        GUI.Label(new Rect(10, 30, 120, 20), orbs);
        GUI.Label(new Rect(10, 50, 120, 20), life);
    }
}
