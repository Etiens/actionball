﻿using UnityEngine;
using System.Collections;

/// MouseLook rotates the transform based on the mouse delta.
/// Minimum and Maximum values can be used to constrain the possible rotation

/// To make an FPS style character:
/// - Create a capsule.
/// - Add a rigid body to the capsule
/// - Add the MouseLook script to the capsule.
///   -> Set the mouse look to use LookX. (You want to only turn character but not tilt it)
/// - Add FPSWalker script to the capsule

/// - Create a camera. Make the camera a child of the capsule. Reset it's transform.
/// - Add a MouseLook script to the camera.
///   -> Set the mouse look to use LookY. (You want the camera to tilt up and down like a head. The character already turns.)

public class CameraController : MonoBehaviour {

    [SerializeField]
    private float m_sensitivityX = 15F;
    [SerializeField]
    private float m_sensitivityY = 15F;
    [SerializeField]
    private float m_minimumX = -360F;
    [SerializeField]
    private float m_maximumX = 360F;
    [SerializeField]
    private float m_minimumY = -60F;
    [SerializeField]
    private float m_maximumY = 60F;
    [SerializeField]
    private float m_maximumZoom = 20F;
    [SerializeField]
    private float m_minimumZoom = 3F;
    [SerializeField]
    private float m_zoomSensitivity = 1F;
    [SerializeField]
    private float m_zoomDistance = 3F;
    [SerializeField]
    private float m_zoomInOutTime = 0.5F;
    [SerializeField]
    private float m_xOffSet = 1F;

    float rotationX = 0F;
    float rotationY = 0F;
    Quaternion originalRotation;
    Quaternion originalArmRotation;
    private float m_originalZoom = 0f;
    private float m_originalOffset = 0f;
    private float m_targetOffset = 0f;
    private float m_targetZoom = 0f;
    private bool m_zooming = false;
    private float m_zoomTimer = 0f;
    private Vector3 smoothPlayerPos;


    [SerializeField]
    Transform m_cameraTransform = null;
    [SerializeField]
    GameObject m_cameraArm = null;

    public GameObject CameraArm { get { return m_cameraArm; } }
    public Transform CameraTransform { get { return m_cameraTransform; } }

    private bool m_cameraRotatesBody = true;
    public bool CameraRotatesBody { get { return m_cameraRotatesBody; } set { m_cameraRotatesBody = value; } }

    void Start()
    {
        Application.targetFrameRate = 120;
        originalRotation = transform.localRotation;
        originalArmRotation = m_cameraArm.transform.localRotation;
        smoothPlayerPos = transform.position;
    }

    void FixedUpdate ()
    {
        // Read the mouse input axis
        rotationX += Input.GetAxis("Mouse X") * m_sensitivityX * Time.fixedDeltaTime;
        rotationY += Input.GetAxis("Mouse Y") * m_sensitivityY * Time.fixedDeltaTime;
        rotationX = ClampAngle (rotationX, m_minimumX, m_maximumX);
        rotationY = ClampAngle (rotationY, m_minimumY, m_maximumY);
        Quaternion xQuaternion = Quaternion.AngleAxis (rotationX, Vector3.up);
        Quaternion yQuaternion = Quaternion.AngleAxis (rotationY, -Vector3.right);
        Quaternion aimRotation = Quaternion.Euler(-rotationY, rotationX, 0);
        if (m_cameraRotatesBody)
        {
            Quaternion lastLocalRot = m_cameraArm.transform.localRotation;
            Quaternion lastRot = transform.rotation;
            m_cameraArm.transform.rotation = originalArmRotation;
            m_cameraArm.transform.localRotation = Quaternion.Slerp(lastLocalRot, originalArmRotation * yQuaternion, 0.5f);
            transform.rotation = Quaternion.Slerp(lastRot, originalRotation * xQuaternion, 0.5f);
        }
        else
        {
            m_cameraArm.transform.rotation = aimRotation;
        }

        // Camera distance
        float cameraDistance = m_cameraTransform.transform.localPosition.z;
        float offset = m_cameraTransform.transform.localPosition.x;
        if (m_zoomTimer > 0)
        {	
            m_zoomTimer -= Time.deltaTime;
            // Ease-in Lerp
            float t = 1 - (m_zoomTimer / m_zoomInOutTime);
            t = Mathf.Sin(t * Mathf.PI * 0.5f);

            cameraDistance = Mathf.Lerp(m_originalZoom, m_targetZoom, t);
            offset = Mathf.Lerp(m_originalOffset, m_targetOffset, t);
        }
        else if (!m_zooming)
        {
            cameraDistance = m_cameraTransform.transform.localPosition.z;
            cameraDistance += Input.GetAxis("Mouse ScrollWheel") * m_zoomSensitivity;
            cameraDistance = Mathf.Clamp(cameraDistance, -m_maximumZoom, -m_minimumZoom);
            offset = 0;
        }

        m_cameraTransform.transform.localPosition = new Vector3(offset, m_cameraTransform.transform.localPosition.y, cameraDistance);


    }

    public void ZoomIn()
    {
        m_originalZoom = m_cameraTransform.transform.localPosition.z;
        m_originalOffset = m_cameraTransform.transform.localPosition.x;
        // camera position is negative ; behind the player
        m_targetZoom = -m_zoomDistance;
        m_targetOffset = m_xOffSet;
        m_zoomTimer = m_zoomInOutTime;
        m_zooming = true;
    }
    public void ZoomOut()
    {
        m_targetZoom = Mathf.Min(m_originalZoom, -m_minimumZoom);
        m_targetOffset = 0;
        m_originalZoom = m_cameraTransform.transform.localPosition.z;
        m_originalOffset = m_cameraTransform.transform.localPosition.x;
        m_zoomTimer = m_zoomInOutTime;
        m_zooming = false;
    }
    public static float ClampAngle (float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }

    void OnGUI()
    {
        if(m_zooming)
        GUI.Label(new Rect(Screen.width * 0.5f, Screen.height * 0.5f, 20, 20), "X");
    }

}
