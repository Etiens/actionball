﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;



public enum NeonColor
{
    NeonWhite,
    NeonGreen,
    NeonPurple,
    NeonYellow,
    NeonBlue,
    NeonBlack,
    None,
}

[RequireComponent(typeof(GameCharacterController))]
public class NeonColorManager : MonoBehaviour
{
    private GameCharacterController m_controller;
    
    private NeonColor m_currentColor;
    public NeonColor CurrentColor { get { return m_currentColor; } }

    private EventWithArg<NeonColor> m_colorChangedEvent = new EventWithArg<NeonColor>();
    public EventWithArg<NeonColor> ColorChangedEvent { get { return m_colorChangedEvent; } }

    private List<NeonColor> m_availableColors = new List<NeonColor>();

    [Header("Available Colors")]
    [SerializeField]
    bool m_neonWhite = true;
    [SerializeField]
    bool m_neonGreen = true;
    [SerializeField]
    bool m_neonBlue = false;
    [SerializeField]
    bool m_neonYellow = false;
    [SerializeField]
    bool m_neonPurple = false;
    [SerializeField]
    bool m_neonBlack = false;

    void Awake()
    {
        m_controller = GetComponent<GameCharacterController>();
        if (m_neonWhite) m_availableColors.Add(NeonColor.NeonWhite);
        if (m_neonGreen) m_availableColors.Add(NeonColor.NeonGreen);
        if (m_neonBlue) m_availableColors.Add(NeonColor.NeonBlue);
        if (m_neonYellow) m_availableColors.Add(NeonColor.NeonYellow);
        if (m_neonPurple) m_availableColors.Add(NeonColor.NeonPurple);
        if (m_neonBlack) m_availableColors.Add(NeonColor.NeonBlack);
        m_currentColor = m_availableColors[0];
    }

    void Start()
    {
        m_colorChangedEvent.Invoke(m_currentColor);
    }

    void Update()
    {
        if (m_controller.CanTakeAction && m_controller.Inputs.NeonColorNext.IsPressed)
        {
            ChangeNeonColor(1);
        }
        else if (m_controller.CanTakeAction && m_controller.Inputs.NeonColorPrevious.IsPressed)
        {
            ChangeNeonColor(-1);
        }
    }

    public void ChangeNeonColor(NeonColor color)
    {
        m_currentColor = color;
        m_colorChangedEvent.Invoke(m_currentColor);
    }

    private void ChangeNeonColor(int direction)
    {
        int index = m_availableColors.IndexOf(m_currentColor);
        index += direction;
        if (index == m_availableColors.Count) index = 0;
        else if (index == -1) index = m_availableColors.Count - 1;
        ChangeNeonColor(m_availableColors[index]);

    }
}
