﻿using UnityEngine;
using System.Collections;


public class GameCharacterController : MonoBehaviour {

    #region States
    [SerializeField]
    private CharacterState m_defaultState = null;
    protected CharacterState m_currentState;
    private CharacterState m_desiredState;
    [SerializeField]
    private float m_desiredStateDuration = 0;
    private float m_desiredStateTimer = 0;
    #endregion // States

    private NeonColorManager m_neonColorManager;
    public NeonColorManager NeonManager { get { return m_neonColorManager; } }


    private InputManager m_inputs = new InputManager();
    public InputManager Inputs { get { return m_inputs; } }

    protected Life m_life;
    public Life CharacterLife { get { return m_life; } }

	Navigation m_navigation;
	public Navigation Navigation { get { return m_navigation; } }

	private bool m_canMove = true;
    public bool CanTakeAction
    {
		get { return m_canMove;} set { m_canMove = value; } 
    }

    private bool m_canHover = true;
    public bool CanHover
	{
		get { return m_canHover;} set { m_canHover = value; } 
    }


    /* ******************************************************* */
    protected virtual void Awake()
    {
		m_currentState = m_defaultState;
		m_navigation = GetComponent<Navigation>();
        m_neonColorManager = GetComponent<NeonColorManager>();
        m_life = GetComponent<Life>();
        m_life.LifeChanged.AddListener(OnLifeChanged);
    }

    protected virtual void Start()
    {
		m_defaultState.EnterState();
    }

    protected virtual void Update()
    {
        if (!m_life.IsDead)
        {
            m_inputs.Update();
        }

        if (m_desiredStateTimer > 0)
        {
            m_desiredStateTimer -= Time.deltaTime;
            if (m_desiredStateTimer < 0)
            {
                m_desiredState = null;
            }
        }
    }

	public bool ChangeState(CharacterState newState)
	{
//		if(newState.GetType() != m_currentState.GetType())
		{
			if (newState.CanEnterState && m_currentState.CanExitState)
			{
				m_currentState.ExitState();

				newState.EnterState();

				m_currentState = newState;
				return true;
            }
            m_desiredState = newState;
            m_desiredStateTimer = m_desiredStateDuration;
        }
        return false;
	}

	public void OnExitState(CharacterState newState = null)
	{
		if (newState && ChangeState(newState))
		{
			;
		}
		else if (m_desiredState && ChangeState(m_desiredState))
		{
			m_desiredState = null;
		} 
		else
		{
			ChangeState(m_defaultState);
		}
	}

    public virtual void OnLifeChanged(LifeChangedEventArgs args)
    {
        if (m_life.IsDead)
        {
            Inputs.Reset();
        }
    }


}
