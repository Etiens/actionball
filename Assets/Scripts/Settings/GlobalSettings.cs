﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class NeonColorSettings
{
    public Color NeonYellow;
    public Color NeonBlue;
    public Color NeonGreen;
    public Color NeonWhite;
    public Color NeonPurple;
    public Color NeonBlack;

    public Color GetColor(NeonColor color)
    {
        switch (color)
        {
            case NeonColor.NeonWhite: return NeonWhite;
            case NeonColor.NeonGreen: return NeonGreen;
            case NeonColor.NeonBlue: return NeonBlue; 
            case NeonColor.NeonYellow: return NeonYellow;
            case NeonColor.NeonPurple: return NeonPurple;
            case NeonColor.NeonBlack: return NeonBlack;
            default: return Color.white; 

        }
    }
}

public class GlobalSettings : Singleton<GlobalSettings>
{
    [SerializeField]
    private NeonColorSettings m_neonColorSettings = null;
    public NeonColorSettings NeonColorSettings { get { return m_neonColorSettings; } }

}
