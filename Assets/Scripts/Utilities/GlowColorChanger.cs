﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Renderer))]
public class GlowColorChanger : MonoBehaviour
{

    float m_colorTimer = 0f;
    float m_colorFadeDuration = 0f;
    bool m_colorFade = false;
    private Color m_fadeStartColor = new Color();

    float m_fadeToColorTimer = 0f;
    float m_fadeToColorDuration = 0f;
    bool m_fadeToColor = false;
    bool m_fadeToBlack = false;
    Color m_previousColor = new Color();

    float m_glowTimer = 0f;
    float m_glowFadeDuration = 0f;
    bool m_glowFade = false;
    float m_fadeStartGlow = 0f;

    float m_fadeToGlowTimer = 0f;
    float m_fadeToGlowDuration = 0f;
    bool m_fadeToGlow = false;
    bool m_fadeToBlackGlow = false;
    float m_previousGlow = 0f;


    private Renderer m_renderer;
    private MaterialPropertyBlock m_materialBlock;
    private Life m_life;

    [SerializeField]
    private bool m_scaleColorWithLife = false;

    [SerializeField]
    private Color m_deadColor = new Color(1f, 0f, 0f, 1f);
    private Color m_currentColor = new Color();
    private float m_currentGlow = 0f;

    public Color BaseColor = new Color(0, 0, 0, 0);
    public float BaseGlow = 1f;

    public Color CurrentGlowColor { get { return m_materialBlock.GetVector("_MKGlowColor"); } }
    public float CurrentGlowPower { get { return m_materialBlock.GetFloat("_MKGlowPower"); } }


    void Awake()
    {
        m_renderer = GetComponent<Renderer>();
        m_materialBlock = new MaterialPropertyBlock();
        m_renderer.SetPropertyBlock(m_materialBlock);
        m_life = GetComponentInParent<Life>();
        ResetColor();
    }

    void Update()
    {
        UpdateGlowColor();
        UpdateGlowPower();
        if (m_scaleColorWithLife && m_life)
        {
            // We set the color directly so that we don't change the current color;
            m_materialBlock.SetColor("_MKGlowColor", Color.Lerp(m_deadColor, m_currentColor, m_life.CurrentLife / m_life.MaxLife));
            m_renderer.SetPropertyBlock(m_materialBlock);
        }
    }

    private void UpdateGlowColor()
    {
        if (m_fadeToColor && m_fadeToColorTimer > 0)
        {
            m_fadeToColorTimer -= Time.deltaTime;
            if (m_fadeToBlack)
            {
                float halfDuration = m_fadeToColorDuration * 0.5f;

                if (m_fadeToColorTimer > halfDuration)
                {
                    SetShaderGlowColor(Color.Lerp(new Color(0, 0, 0, 0), m_previousColor, (m_fadeToColorTimer - halfDuration) / halfDuration));
                }
                else
                {
                    SetShaderGlowColor(Color.Lerp(m_fadeStartColor, new Color(0, 0, 0, 0), m_fadeToColorTimer / halfDuration));
                }
            }
            else
            {
                SetShaderGlowColor(Color.Lerp(m_fadeStartColor, m_previousColor, m_fadeToColorTimer / m_fadeToColorDuration));
            }
        }
        else if (m_colorTimer > 0)
        {
            m_fadeToColor = false;
            m_colorTimer -= Time.deltaTime;
            if (m_colorFade)
            {
                SetShaderGlowColor(Color.Lerp(BaseColor, m_fadeStartColor, m_colorTimer / m_colorFadeDuration));
            }
            if (m_colorTimer < 0)
            {
                ResetColor();
            }
        }
    }

    private void UpdateGlowPower()
    {
        if (m_fadeToGlow && m_fadeToGlowTimer > 0)
        {
            m_fadeToGlowTimer -= Time.deltaTime;
            if (m_fadeToBlackGlow)
            {
                float halfDuration = m_fadeToGlowDuration * 0.5f;

                if (m_fadeToGlowTimer > halfDuration)
                {
                    SetShaderGlowPower(Mathf.Lerp(0, m_previousGlow, (m_fadeToGlowTimer - halfDuration) / halfDuration));
                }
                else
                {
                    SetShaderGlowPower(Mathf.Lerp(m_fadeStartGlow, 0f, m_fadeToGlowTimer / halfDuration));
                }
            }
            else
            {
                SetShaderGlowPower(Mathf.Lerp(m_fadeStartGlow, m_previousGlow, m_fadeToGlowTimer / m_fadeToGlowDuration));
            }
        }
        else if (m_glowTimer > 0)
        {
            m_fadeToGlow = false;
            m_glowTimer -= Time.deltaTime;
            if (m_glowFade)
            {
                SetShaderGlowPower(Mathf.Lerp(BaseGlow, m_fadeStartGlow, m_glowTimer / m_glowFadeDuration));
            }
            if (m_glowTimer < 0)
            {
                ResetColor();
            }
        }
    }

    public void ChangeGlowColor(Color color, float colorDuration = 0f, bool fadeForDuration = false, bool fadeToColor = false, bool fadeToBlack = false, float fadeToColorDuration = 0f)
    {
        if (fadeToColor)
        {
            m_fadeToColorTimer = fadeToColorDuration;
            m_fadeToColor = true;
            m_fadeToBlack = fadeToBlack;
            m_fadeToColorDuration = fadeToColorDuration;
            m_previousColor = m_materialBlock.GetVector("_MKGlowColor");
        }
        else
        {
            SetShaderGlowColor(color);
        }
        m_fadeStartColor = color;
        m_colorFade = fadeForDuration;
        m_colorTimer = colorDuration;
        m_colorFadeDuration = colorDuration;
    }

    public void ResetColor()
    {
        Color currentColor = m_materialBlock.GetVector("_MKGlowColor");
        if (currentColor != BaseColor)
        {
            m_colorTimer = 0;
            SetShaderGlowColor(BaseColor);
        }
    }

    public void ResetGlow()
    {
        float currentGlow = m_materialBlock.GetFloat("_MKGlowPower");
        if (currentGlow != BaseGlow)
        {
            m_glowTimer = 0;
            SetShaderGlowPower(BaseGlow);
        }
    }

    private void SetShaderGlowColor(Color color)
    {
        m_materialBlock.SetColor("_MKGlowColor", color);
        m_renderer.SetPropertyBlock(m_materialBlock);
        m_currentColor = color;
    }

    public void ChangeGlowPower(float power, float powerDuration = 0f, bool fadeGlowForDuration = false, bool fadeToPower = false, bool fadeGlowToBlack = false, float fadeToGlowDuration = 0f)
    {
        if (fadeToPower)
        {
            m_fadeToGlowTimer = fadeToGlowDuration;
            m_fadeToGlow = true;
            m_fadeToBlackGlow = fadeGlowToBlack;
            m_fadeToGlowDuration = fadeToGlowDuration;
            m_previousGlow = m_materialBlock.GetFloat("_MKGlowPower");
        }
        else
        {
            SetShaderGlowPower(power);
        }
        m_fadeStartGlow = power;
        m_glowFade = fadeGlowForDuration;
        m_glowTimer = powerDuration;
        m_glowFadeDuration = powerDuration;
    }

    private void SetShaderGlowPower(float glow)
    {
        m_materialBlock.SetFloat("_MKGlowPower", glow);
        m_renderer.SetPropertyBlock(m_materialBlock);
        m_currentGlow = glow;
    }
}
