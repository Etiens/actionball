﻿using System.Collections.Generic;

public class EventWithArg<T>
{
    public delegate void Callback(T arg);
    List<Callback> m_listeners = new List<Callback>();

    public void AddListener(Callback callback)
    {
        m_listeners.Add(callback);
    }

    public void Invoke(T args)
    {
        for (int i = 0; i < m_listeners.Count; i++)
        {
            m_listeners[i](args);
        }
    }
}
